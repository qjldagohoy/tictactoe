var block1;
var block2;
var block3;
var block4;
var block5;
var block6;
var block7;
var block8;
var block9;

let block1M = 0;
let block2M = 0;
let block3M = 0;
let block4M = 0;
let block5M = 0;
let block6M = 0;
let block7M = 0;
let block8M = 0;
let block9M = 0;

let moveCount = 0;
let turn = 0;
let curr = 1;

let userScore = 0;
let computerScore = 0;
let drawScore = 0;
const userScore_span = document.getElementById("user-score");
const computerScore_span = document.getElementById("computer-score");
const drawScore_span = document.getElementById("draw-score");
const scoreBoard_div = document.querySelector(".score-board");

function getValue() {
    block1 = document.tictactoe.block1.value;
    block2 = document.tictactoe.block2.value;
    block3 = document.tictactoe.block3.value;
    block4 = document.tictactoe.block4.value;
    block5 = document.tictactoe.block5.value;
    block6 = document.tictactoe.block6.value;
    block7 = document.tictactoe.block7.value;
    block8 = document.tictactoe.block8.value;
    block9 = document.tictactoe.block9.value;
}

function p1Alert() {
    alert("Player 1 wins!");
    reset();
}

function p2Alert() {
    alert("Player 2 wins!");
    reset();
}

function reset() {
    document.tictactoe.block1.value = "     ";
    document.tictactoe.block2.value = "     ";
    document.tictactoe.block3.value = "     ";
    document.tictactoe.block4.value = "     ";
    document.tictactoe.block5.value = "     ";
    document.tictactoe.block6.value = "     ";
    document.tictactoe.block7.value = "     ";
    document.tictactoe.block8.value = "     ";
    document.tictactoe.block9.value = "     ";
    block1M = 0;
    block2M = 0;
    block3M = 0;
    block4M = 0;
    block5M = 0;
    block6M = 0;
    block7M = 0;
    block8M = 0;
    block9M = 0;
    getValue();
    turn = 0;
    moveCount = 0;
}

function loseAlert() {
    computerScore++;
    computerScore_span.innerHTML = computerScore;
    reset();
    const userChoice_div = scoreBoard_div;
	scoreBoard_div.style.transition="all 0.3s";
    userChoice_div.classList.add('redglow');
    setTimeout(() => userChoice_div.classList.remove('redglow'),1500);
}

function winAlert() {
    userScore++;
    userScore_span.innerHTML = userScore;
    reset();
    const userChoice_div = scoreBoard_div;
    scoreBoard_div.style.transition="all 0.3s";
    userChoice_div.classList.add('greenglow');
    setTimeout(() => userChoice_div.classList.remove('greenglow'),1500);
}

function drawCheck() {
    getValue()
    moveCount = block1M + block2M + block3M + block4M + block5M + block6M + block7M + block8M + block9M 
    if(moveCount == 9) {
        drawScore++;
        drawScore_span.innerHTML = drawScore;
        reset();
        const userChoice_div = scoreBoard_div;
	    scoreBoard_div.style.transition="all 0.3s";
        userChoice_div.classList.add('grayglow');
        setTimeout(() => userChoice_div.classList.remove('grayglow'),1500);
    }
}

function resetScore() {
    if(confirm("Are you sure to start a new game? ")){
        reset();
        userScore = 0;
        computerScore = 0;
        drawScore = 0;
        userScore_span.innerHTML = userScore;
        computerScore_span.innerHTML = computerScore;
        drawScore_span.innerHTML = drawScore;    
    } else {
        null();
    }	
}

function winCheck() {
    check2();
    
    //123
    if(block1 === " O " && block2 === " O " && block3M === 0 && turn === 1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;
    } else if(block2 == " O " && block3 == " O " && block1M == 0 && turn == 1) {
        document.tictactoe.block1.value = " O ";
        block1M = 1;
        turn = 0;
    } else if(block1 == " O " && block3 == " O " && block2M == 0 && turn == 1) {
        document.tictactoe.block2.value = " O ";
        block2M = 1;
        turn = 0;

    //456
    } else if(block4 == " O " && block5 == " O " && block6M == 0 && turn == 1) {
        document.tictactoe.block6.value = " O ";
        block6M = 1;
        turn = 0;
    } else if(block5 == " O " && block6 == " O " && block4M == 0 && turn == 1) {
        document.tictactoe.block4.value = " O ";
        block4M = 1;
        turn = 0;
    } else if(block4 == " O " && block6 == " O " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;
    
    //789
    } else if(block7 == " O " && block8 == " O " && block9M == 0 && turn == 1) {
        document.tictactoe.block9.value = " O ";
        block9M = 1;
        turn = 0;
    } else if(block8 == " O " && block9 == " O " && block7M == 0 && turn == 1) {
        document.tictactoe.block7.value = " O ";
        block7M = 1;
        turn = 0;
    } else if(block7 == " O " && block9 == " O " && block8M == 0 && turn == 1) {
        document.tictactoe.block8.value = " O ";
        block8M = 1;
        turn = 0;
    
    //159
    } else if(block1 == " O " && block5 == " O " && block9M == 0 && turn == 1) {
        document.tictactoe.block9.value = " O ";
        block9M = 1;
        turn = 0;
    } else if(block5 == " O " && block9 == " O " && block1M == 0 && turn == 1) {
        document.tictactoe.block1.value = " O ";
        block1M = 1;
        turn = 0;
    } else if(block1 == " O " && block9 == " O " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;

    //357
    } else if(block3 == " O " && block5 == " O " && block7M == 0 && turn == 1) {
        document.tictactoe.block7.value = " O ";
        block7M = 1;
        turn = 0;
    } else if(block7 == " O " && block5 == " O " && block3M == 0 && turn == 1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;
    } else if(block3 == " O " && block7 == " O " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;
    
    //147   
    } else if(block1 == " O " && block7 == " O " && block4M == 0 && turn == 1) {
        document.tictactoe.block4.value = " O ";
        block4M = 1;
        turn = 0;
    } else if(block4 == " O " && block7 == " O " && block1M == 0 && turn == 1) {
        document.tictactoe.block1.value = " O ";
        block1M = 1;
        turn = 0;
    } else if(block1 == " O " && block4 == " O " && block7M == 0 && turn == 1) {
        document.tictactoe.block7.value = " O ";
        block7M = 1;
        turn = 0;

    //258
    } else if(block2 == " O " && block8 == " O " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;
    } else if(block5 == " O " && block8 == " O " && block2M == 0 && turn == 1){
        document.tictactoe.block2.value = " O ";
        block2M = 1;
        turn = 0;
    } else if(block2 == " O " && block5 == " O " && block8M == 0 && turn == 1) {
        document.tictactoe.block8.value = " O ";
        block8M = 1;
        turn = 0;
    

    //369
    } else if(block3 == " O " && block9 == " O " && block6M == 0 && turn == 1) {
        document.tictactoe.block6.value = " O ";
        block6M = 1;
        turn = 0;
    } else if(block6 == " O " && block9 == " O " && block3M == 0 && turn == 1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;
    } else if(block3 == " O " && block6 == " O " && block9M == 0 && turn == 1) {
        document.tictactoe.block9.value = " O ";
        block9M = 1;
        turn = 0;
    } else {
        comCheck();
    }
    check2();
}

function comCheck() {
    check2();
    //123
    if(block1 == " X " && block2 == " X " && block3M == 0 && turn == 1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;
    } else if(block2 == " X " && block3 == " X " && block1M == 0 && turn == 1) {
        document.tictactoe.block1.value = " O ";
        block1M = 1;
        turn = 0;
    } else if(block1 == " X " && block3 == " X " && block2M == 0 && turn == 1) {
        document.tictactoe.block2.value = " O ";
        block2M = 1;
        turn = 0;

    //456
    } else if(block4 == " X " && block5 == " X " && block6M == 0 && turn == 1) {
        document.tictactoe.block6.value = " O ";
        block6M = 1;
        turn = 0;
    } else if(block5 == " X " && block6 == " X " && block4M == 0 && turn == 1) {
        document.tictactoe.block4.value = " O ";
        block4M = 1;
        turn = 0;
    } else if(block4 == " X " && block6 == " X " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;
    
    //789
    } else if(block7 == " X " && block8 == " X " && block9M == 0 && turn == 1) {
        document.tictactoe.block9.value = " O ";
        block9M = 1;
        turn = 0;
    } else if(block8 == " X " && block9 == " X " && block7M == 0 && turn == 1) {
        document.tictactoe.block7.value = " O ";
        block7M = 1;
        turn = 0;
    } else if(block7 == " X " && block9 == " X " && block8M == 0 && turn == 1) {
        document.tictactoe.block8.value = " O ";
        block8M = 1;
        turn = 0;
    
    //159
    } else if(block1 == " X " && block5 == " X " && block9M == 0 && turn == 1) {
        document.tictactoe.block9.value = " O ";
        block9M = 1;
        turn = 0;
    } else if(block5 == " X " && block9 == " X " && block1M == 0 && turn == 1) {
        document.tictactoe.block1.value = " O ";
        block1M = 1;
        turn = 0;
    } else if(block1 == " X " && block9 == " X " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;

    //357
    } else if(block3 == " X " && block5 == " X " && block7M == 0 && turn == 1) {
        document.tictactoe.block7.value = " O ";
        block7M = 1;
        turn = 0;
    } else if(block7 == " X " && block5 == " X " && block3M == 0 && turn == 1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;
    } else if(block3 == " X " && block7 == " X " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;
    
    //147   
    } else if(block1 == " X " && block7 == " X " && block4M == 0 && turn == 1) {
        document.tictactoe.block4.value = " O ";
        block4M = 1;
        turn = 0;
    } else if(block4 == " X " && block7 == " X " && block1M == 0 && turn == 1) {
        document.tictactoe.block1.value = " O ";
        block1M = 1;
        turn = 0;
    } else if(block1 == " X " && block4 == " X " && block7M == 0 && turn == 1) {
        document.tictactoe.block7.value = " O ";
        block7M = 1;
        turn = 0;

    //258
    } else if(block2 == " X " && block8 == " X " && block5M == 0 && turn == 1) {
        document.tictactoe.block5.value = " O ";
        block5M = 1;
        turn = 0;
    } else if(block5 == " X " && block8 == " X " && block2M == 0 && turn == 1){
        document.tictactoe.block2.value = " O ";
        block2M = 1;
        turn = 0;
    } else if(block2 == " X " && block5 == " X " && block8M == 0 && turn == 1) {
        document.tictactoe.block8.value = " O ";
        block8M = 1;
        turn = 0;

    //win flaw
    } else if(block3 == " X " && block8 == " X " && block6M == 0 && turn == 1) {
        document.tictactoe.block6.value = " O ";
        block6M = 1;
        turn = 0;
    } else if(block3 === " X " && block7 == " X " && block6M == 0 && turn ==1) {
        document.tictactoe.block6.value = " O ";
        block6M = 1; 
        turn = 0;
    } else if(block5 === " X " && block9 == " X " && block3M == 0 && turn ==1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;
    } else if(block6 === " X " && block8 == " X " && block3M == 0 && turn ==1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;

    //369
    } else if(block3 == " X " && block9 == " X " && block6M == 0 && turn == 1) {
        document.tictactoe.block6.value = " O ";
        block6M = 1;
        turn = 0;
    } else if(block6 == " X " && block9 == " X " && block3M == 0 && turn == 1) {
        document.tictactoe.block3.value = " O ";
        block3M = 1;
        turn = 0;
    } else if(block3 == " X " && block6 == " X " && block9M == 0 && turn == 1) {
        document.tictactoe.block9.value = " O ";
        block9M = 1;
        turn = 0;
    } else {
        ai();
    }
    check2();
}

function ai() {
    getValue();
    
    if(document.tictactoe.block5.value == "     " && turn == 1) {
        document.tictactoe.block5.value = " O ";
        turn = 0;
        block5M = 1;
    } else if(document.tictactoe.block1.value == "     " && turn == 1) {
        document.tictactoe.block1.value = " O ";
        turn = 0;
        block1M = 1;
    } else if(document.tictactoe.block9.value == "     " && turn == 1) {
        document.tictactoe.block9.value = " O ";
        turn = 0;
        block9M = 1;
    } else if(document.tictactoe.block6.value == "     " && turn == 1) {
        document.tictactoe.block6.value = " O ";
        turn = 0;
        block6M = 1;
    } else if(document.tictactoe.block2.value == "     " && turn == 1) {
        document.tictactoe.block2.value = " O ";
        turn = 0;
        block2M = 1;
    } else if(document.tictactoe.block8.value == "     " && turn == 1) {
        document.tictactoe.block8.value = " O ";
        turn = 0;
        block8M = 1;
    } else if(document.tictactoe.block3.value == "     " && turn == 1) {
        document.tictactoe.block3.value = " O ";
        turn = 0;
        block3M = 1;
    } else if(document.tictactoe.block7.value == "     " && turn == 1) {
        document.tictactoe.block7.value = " O ";
        turn = 0;
        block7M = 1;
    } else if(document.tictactoe.block4.value == "     " && turn == 1) {
        document.tictactoe.block4.value = " O ";
        turn = 0;
        block4M = 1;
    }
    check2();
}

function check2() {
    getValue();
    drawCheck();
    if(block1 == " O " && block2 == " O " && block3 == " O ") {
        loseAlert();
    } else if(block4 == " O " && block5 == " O " && block6 == " O ") {
        loseAlert();
    } else if(block7 == " O " && block8 == " O " && block9 == " O ") {
        loseAlert();
    } else if(block1 == " O " && block5 == " O " && block9 == " O ") {
        loseAlert();
    } else if(block1 == " O " && block4 == " O " && block7 == " O ") {
        loseAlert();
    } else if(block2 == " O " && block5 == " O " && block8 == " O ") {
        loseAlert();
    } else if(block3 == " O " && block6 == " O " && block9 == " O ") {
        loseAlert();
    } else if(block1 == " O " && block5 == " O " && block9 == " O ") {
        loseAlert();
    } else if(block3 == " O " && block5 == " O " && block7 == " O ") {
        loseAlert();
    }
}

function check1() {
    if(block1 == " X " && block2 == " X " && block3 == " X ") {
        winAlert();
    } else if(block4 == " X " && block5 == " X " && block6 == " X ") {
        winAlert();
    } else if(block7 == " X " && block8 == " X " && block9 == " X ") {
        winAlert();
    } else if(block1 == " X " && block5 == " X " && block9 == " X ") {
        winAlert();
    } else if(block1 == " X " && block4 == " X " && block7 == " X ") {
        winAlert();
    } else if(block2 == " X " && block5 == " X " && block8 == " X ") {
        winAlert();
    } else if(block3 == " X " && block6 == " X " && block9 == " X ") {
        winAlert();
    } else if(block1 == " X " && block5 == " X " && block9 == " X ") {
        winAlert();
    } else if(block3 == " X " && block5 == " X " && block7 == " X ") {
        winAlert();
    } else {
        winCheck();
        check2();
        drawCheck();
    }  
}

function p1Check() {
    if(block1 == " X " && block2 == " X " && block3 == " X ") {
        p1Alert();
    } else if(block4 == " X " && block5 == " X " && block6 == " X ") {
        p1Alert();
    } else if(block7 == " X " && block8 == " X " && block9 == " X ") {
        p1Alert();
    } else if(block1 == " X " && block5 == " X " && block9 == " X ") {
        p1Alert();
    } else if(block1 == " X " && block4 == " X " && block7 == " X ") {
        p1Alert();
    } else if(block2 == " X " && block5 == " X " && block8 == " X ") {
        p1Alert();
    } else if(block3 == " X " && block6 == " X " && block9 == " X ") {
        p1Alert();
    } else if(block3 == " X " && block5 == " X " && block7 == " X ") {
        p1Alert();
    } else {
        p2Check();
        drawCheck();
    } 
}

function p2Check() {
    getValue();
    drawCheck();
    if(block1 == " O " && block2 == " O " && block3 == " O ") {
        p2Alert();
    } else if(block4 == " O " && block5 == " O " && block6 == " O ") {
        p2Alert();
    } else if(block7 == " O " && block8 == " O " && block9 == " O ") {
        p2Alert();
    } else if(block1 == " O " && block5 == " O " && block9 == " O ") {
        p2Alert();
    } else if(block1 == " O " && block4 == " O " && block7 == " O ") {
        p2Alert();
    } else if(block2 == " O " && block5 == " O " && block8 == " O ") {
        p2Alert();
    } else if(block3 == " O " && block6 == " O " && block9 == " O ") {
        p2Alert();
    } else if(block3 == " O " && block5 == " O " && block7 == " O ") {
        p2Alert();
    }
}

function block1Click() {
    if(document.tictactoe.block1.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block1.value = ' X '; 
        block1M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block1.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block1.value = ' X '; 
        block1M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block1.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block1.value = ' O '; 
        block1M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block2Click() {
    if(document.tictactoe.block2.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block2.value = ' X '; 
        block2M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block2.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block2.value = ' X '; 
        block2M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block2.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block2.value = ' O '; 
        block2M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block3Click() {
    if(document.tictactoe.block3.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block3.value = ' X '; 
        block3M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block3.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block3.value = ' X '; 
        block3M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block3.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block3.value = ' O '; 
        block3M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block4Click() {
    if(document.tictactoe.block4.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block4.value = ' X '; 
        block4M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block4.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block4.value = ' X '; 
        block4M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block4.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block4.value = ' O '; 
        block4M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block5Click() {
    if(document.tictactoe.block5.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block5.value = ' X '; 
        block5M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block5.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block5.value = ' X '; 
        block5M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block5.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block5.value = ' O '; 
        block5M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block6Click() {
    if(document.tictactoe.block6.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block6.value = ' X '; 
        block6M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block6.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block6.value = ' X '; 
        block6M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block6.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block6.value = ' O '; 
        block6M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block7Click() {
    if(document.tictactoe.block7.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block7.value = ' X '; 
        block7M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block7.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block7.value = ' X '; 
        block7M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block7.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block7.value = ' O '; 
        block7M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block8Click() {
    if(document.tictactoe.block8.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block8.value = ' X '; 
        block8M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block8.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block8.value = ' X '; 
        block8M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block8.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block8.value = ' O '; 
        block8M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

function block9Click() {
    if(document.tictactoe.block9.value == '     ' && turn == 0 && curr == 1) {
        document.tictactoe.block9.value = ' X '; 
        block9M = 1; 
        turn = 1; 
        getValue(); 
        check1();
    } else if(document.tictactoe.block9.value == '     ' && turn == 1 && curr == 2) {
        document.tictactoe.block9.value = ' X '; 
        block9M = 1; 
        turn = 0; 
        getValue(); 
        p1Check();
    } else if(document.tictactoe.block9.value == '     ' && turn == 0 && curr == 2) {
        document.tictactoe.block9.value = ' O '; 
        block9M = 1; 
        turn = 1; 
        getValue(); 
        p1Check();
    } 
    drawCheck();
}

