# TicTacToe

A Tic Tac Toe game application

1. Download the files, tic_tac_toe.html, TTTapp.js, and TTTstyles.css.
2. Copy the downloaded files and saved it onto a single folder.
3. Open tic_tac_toe.html.

Mechanics

SYMBOLS: [X - Player]      [O - Computer]

1. Player will be the first to make a move.
2. Player and Computer will take turns until one of them wins, loses, or get a draw. 
3. First player to get 3 of their corresponding symbols in a row, column or diagonal will win.
4. If the player wins, it will add up to the User score and a green flash will light up on the score board.
5. If the computer wins, it will add up to the Comp score and a red flash will light up on the score board.
6. If Player vs. Computer gets a draw then will it tally a score for the Draw score and a gray flash will 
   light up on the score board.
7. You can click the reset button below to restart the game/scoreboard.
8. Enjoy and beat the AI. Goodluck :)